import React, { Component } from 'react';

import classNames from 'classnames';

import { connect } from 'react-redux';

import { changeTab } from './actions';

class App extends Component {
  render() {

    const { data, active } = this.props;

    return (
      <div className="container welcome_full_screen">
        <div className="row mt5vh">
          <div className="col-md-3">
            <ul className="nav nav-pills about">
              {data.map(item => (<li key={item.key} className={classNames({ active: item.key === active })}>
                <a onClick={() => this.props.changeTab(item.key)} data-toggle="pill" href={`#${item.key}`}>{item.tab}</a>
              </li>))}
            </ul>
          </div>

          <div className="col-md-8">
            <div className="tab-content about">

              {data.map(item => (
                <div key={item.key} id={item.key} className={classNames('tab-pane fade', { 'active in': item.key === active })}>
                  <h3>{item.heading}</h3>
                  <div dangerouslySetInnerHTML={{ __html: item.content }}></div>
                </div>
              ))}

            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ active, data }) => ({ active, data });

const mapDispathToProps = (dispath) => ({
  changeTab: key => dispath(changeTab(key))
});

export default connect(mapStateToProps, mapDispathToProps)(App);
