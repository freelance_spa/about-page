
const initState = {
	active: 'about',
	data: [
		{
			key: 'about',
			tab: 'ABOUT SILKSTAQ',
			heading: 'About',
			content: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam placerat, neque et viverra elementum, nulla dolor pretium sapien, eu eleifend ante ipsum ut ex. Duis sit amet vulputate est. Donec ac sagittis nisi. Nulla elementum nunc eu ante hendrerit consequat.</p><p>Cras bibendum congue ex quis gravida. Proin elementum neque a quam ultricies, consectetur pellentesque tellus tempor. Praesent eu orci congue leo cursus facilisis. Aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam placerat, neque et viverra elementum, nulla dolor pretium sapien, eu eleifend ante ipsum ut ex. Duis sit amet vulputate est. Donec ac sagittis nisi. Nulla elementum nunc eu ante hendrerit consequat.</p><p>Cras bibendum congue ex quis gravida. Proin elementum neque a quam ultricies, consectetur pellentesque tellus tempor. Praesent eu orci congue leo cursus facilisis. Aliquam erat volutpat.</p><p><img src="http://mspmag.com/downloads/33229/download/Spring-Fashion-Influencers.jpg?cb=dd09bc14bc9928f05a8f57d88dfc8ede" alt="" /></p>'
		},
		{
			key: 'how',
			tab: 'HOW SILKSTAQ WORKS',
			heading: 'How silkstaq works',
			content: '<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>'
		},
		{
			key: 'faqs',
			tab: 'FAQs',
			heading: 'Frequently asked questions',
			content: '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>'
		},
		{
			key: 'legal',
			tab: 'LEGAL',
			heading: 'Terms and Conditions',
			content: '<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>'
		},
		{
			key: 'contact',
			tab: 'CONTACT US',
			heading: 'Contact us',
			content: '<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>'
		}
	]
}

const rootStore = (state = initState, action) => {
	switch (action.type) {
		case 'CHANGE_TAB':
			const { key } = action;
			return { ...state, active: key };
		default:
			return state;
	}
}

export default rootStore;